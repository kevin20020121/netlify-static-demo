const fs = require('fs');
const path = require('path');
const folder_site = path.resolve(__dirname, '../../js/');
const file_site = path.resolve(__dirname, '../../js/dist.js');
const html_site = path.resolve(__dirname, '../static/_index.html');
const dist_html_site = path.resolve(__dirname, '../../index.html');

require('dotenv').config();

if (fs.existsSync(folder_site)) {
  fs.rmdirSync(folder_site, {
    recursive: true
  });
}

fs.mkdirSync(folder_site);

fs.appendFileSync(file_site, `(function() { console.log('${process.env.GENERATE_SOURCEMAP};, ${process.env.NODE_ENV}') })();`);

const html_desc = fs.readFileSync(html_site, 'utf-8');

if (fs.existsSync(dist_html_site)) {
  fs.unlinkSync(dist_html_site);
}

const append_js_file_to_HTML = html_desc.replace('<title>Hello</title>', '<title>Hello</title>\n<script src="./js/dist.js"></script>');

fs.appendFileSync(dist_html_site, append_js_file_to_HTML);

require('dotenv').config();
